package com.binar.day4;

import java.util.ArrayList;
import java.util.List;

public class StringMethod {
    public static void main(String[] args) {
//        String name = "apa kabar";
//        System.out.println("charAt=" + name.charAt(4));
//        System.out.println("Length=" + name.length());
//        System.out.println("index Of=" + name.indexOf('a'));
//        intArraytringValue();
        mathodRecursive(10);
    }

    public static void stringValue() {
        String namaTeman1 = "Sabrina";
        String namaTeman2 = "Asep";
        String namaTeman3 = "Ucok";
        String namaTeman4 = "Nassar";
        String namaTeman5 = "Jarjit";
    }

    public static void intArraytringValue() {
        int y[] = new int[10];
        int x[] = {100, 200, 300};
        int a = x[0] + x[2];
        x[1] = a + 100;
        int numElemen = x.length;

        y[0] = 200;
        System.out.println("Value =" + y[0]);

    }

    public static void modulus() {
        int hasilBagi = 7 % 2;
//        System.out.println(hasilBagi);

        int unaryChek = 10;
//        System.out.println(--unaryChek);  // 9 : 10-1
//        System.out.println(++unaryChek); // 9 + 1 = 10
        System.out.println(unaryChek++); // 10 1
        System.out.println(unaryChek + 5); // 10 + 5  = 10
        System.out.println(unaryChek + 7); // 11 = 10 + 7  = 10

//        System.out.println(unaryChek);
        for (int i = 1; i < 5; i++) {

        }
    }

    public static void assigment() {
        int a = 10;
        int b = a + 10;
        a = a - 5;
        System.out.println(a);
    }

    public static void operatorPerbandingan() {
        int a = 21;
        int b = 20;

        System.out.println(a == b);


        int saldoSaya = 10000;
        int belanjaTotal = 20000;
        if (saldoSaya > belanjaTotal) {

        } else {

        }


        // string?
        String textEquals = "buku";
        System.out.println(textEquals.equals("bUku"));


    }

    public static void logicalMethod() {
        int umur = 30;
        float nilai = 3.8F;

        System.out.println((umur >= 25) && (nilai <= 5));

        String nilaiSaya = "A";

        char nilaiSayaChar = 'A';

        System.out.println(nilaiSayaChar == 'A');

        System.out.println((nilaiSaya.equals("B") ? "nilai saya adalah B" :
                ((nilaiSaya.equals("C") ? "nilai adalah C" :
                        ((nilaiSaya.equals("A") ? "nilai adalah A" : "tidak punya nilai "
                        ))))));
        int chek = 1000;
        switch (chek) {
            case 1000:
                System.out.println("1000");
                break;
            case 2000:
                System.out.println("2000");
                break;
            case 3000:
                System.out.println("3000");
                break;
            default:
                System.out.println("default value");
                break;
        }

    }

    public static void methodFor() {
        for (int i = 1; i <= 10; i += 3) {

            if (i == 5) {
                continue;
            }

            if (i == 7) {
                break;
            }
            System.out.println("nilainya :" + i);
        }

        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println(" ");
        }
    }

    public static void methodWhile() {
        int angka = 10;
        /*
        10-1 = 9
        9 -1 = 8
        8 - 1 = 7
         */
        while (angka >= 7){
            System.out.println("iya besar ="+10);
            //belum ada limit perbentian
            angka --;
        }

        List<String> listData = new ArrayList<>();
        listData.add("dhea");
        listData.add("michel");
        listData.add("khoiri");
        listData.add("riki aldi pari");

        List<String[]> listData2 = new ArrayList<>();
        String dataNama[] = {"riki","alid","pari"};
        String dataNama2[] = {"riki2","alid2","pari2"};
        listData2.add(dataNama);
        listData2.add(dataNama2);


         // for?
        for (String data : listData) {
            System.out.println(""+data);
        }

        // for?
        for (String[] data : listData2) {
            System.out.println("satu paket array = "+data.toString());
            for(String dataKedua : data){
                System.out.println("array list= "+dataKedua);
            }
        }
    }

    public static void methodDoWhile() {
        int a = 2;
        do{
            // 1. melakukukan eksekusi : walaupun tidak terpenuhi kodisinya
            System.out.println("cetak tampa terpenuhi kondisinya");
        }while (a == 2);
    }

    public static int mathodRecursive (int angka){
        if(angka > 0){
            System.out.println("cetak terus menerus");
            return angka  + mathodRecursive(20);
        }else{
            System.out.println("stok");
        }
        return angka;
    }
}
