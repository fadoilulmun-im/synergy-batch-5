package com.binar.clean.code.service;

import com.binar.clean.code.model.Barang;

import java.util.List;

public interface BarangService {
    public Barang saveBarang(Barang obj);

    public Barang updateBarang(Barang obj);

    public Barang deleteBarang(Barang obj);

    public List<Barang> listBarang();


    public Barang getById(Long id);
}
