package com.binar.typeData;

import org.omg.CORBA.INTERNAL;

import java.math.BigInteger;
import java.util.ArrayList;

public class TypeData {
    public  String nama = "Pensil ";
    public Character charTest =null ;

    public char charType = '2';

//    public char charType2 = null; //

    int int1 = 1;

//    int int2 = null;
    Integer integer3 = null;

    double typeDouble  = 123.98;

    ArrayList<Integer> list = new ArrayList<>();


    public   String simpanBarang(){
        String nama =  "pulpen";
        return nama;
    }

    public String simpanBarang(String nama){
        this.nama = nama; // "buku gambar"
        return this.nama ;
    }

    public  String simpanBarang2(String nama2){
        return this.nama ;
    }

    // method call method:

    public String subMethod(String value ){
        return  simpanBarang(value);
    }

    // Set value di dalam method
    public  String simpanBarang3(String nama3){
        nama3 = "penghapus";
        return  nama3;
    }

    // type data length
    int chekLengthInt = 2147483647;

//    int chekLengthInt2 = 2147483648;

//    long chekLengthLong2 = 2147483648999999999999L;

    BigInteger bi = new BigInteger("2147483648999999999999");

    float typeFloat = 123.0f;

    float typeFloat2 = 123.9887f;

    double typeDouble2 = 123.0;

    double typeDouble3 = 123d;

    double typeDouble4 = 123;

    char ascii2 = '\111';

    char ascii = '\111';

    boolean typeBoolean2;
    boolean typeBoolean = false;
    // primitive
//    int a = null; // primitive
    Integer aNull = null; //

    /*

    int  = Integer
    long = Long
    double = Double
    float = Float

     */
}
